

class WeatherInformation {
  late String name;
  late coordModel model;
  late Main mainModel;
  late wind windModel;
  late List<weather> weatherModel;

  WeatherInformation({
    required this.name,
    required this.model,
    required this.mainModel,
    required this.windModel,
    required this.weatherModel,
  });

  WeatherInformation.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    model = (json['coord'] != null ? new coordModel.fromJson(json['coord']) : null)!;
    mainModel = (json['main'] != null ? new Main.fromJson(json['main']) : null)!;
    windModel = (json['wind'] != null ? new wind.fromJson(json['wind']) : null)!;
    if (json['weather'] != null) {
      weatherModel = [];
      json['weather'].forEach((v) {
        weatherModel.add(new weather.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.model != null) {
      data['Store'] = this.model.toJson();
    }
    if (this.mainModel != null) {
      data['main'] = this.mainModel.toJson();
    }
    if (this.windModel != null) {
      data['wind'] = this.windModel.toJson();
    }
    if (this.weatherModel != null) {
      data['weather'] = this.weatherModel.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class coordModel {
  late double lon;
  late double lat;

  coordModel({required this.lon, required this.lat});

  coordModel.fromJson(Map<String, dynamic> json) {
    lon = json['lon'];
    lat = json['lat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lon'] = this.lon;
    data['lat'] = this.lat;
    return data;
  }
}

class weather {
  late String main;
  late String description;

  weather({required this.main, required this.description});

  weather.fromJson(Map<String, dynamic> json) {
    main = json['main'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['main'] = this.main;
    data['description'] = this.description;
    return data;
  }
}

class Main {
  late double temp;
  late double feels_like;
  late double temp_min;
  late double temp_max;
  late int pressure;
  late int humidity;

  Main({
    required this.temp,
    required this.feels_like,
    required this.temp_min,
    required this.temp_max,
    required this.pressure,
    required this.humidity,
  });

  Main.fromJson(Map<String, dynamic> json) {
    temp = json['temp'];
    feels_like = json['feels_like'];
    temp_min = json['temp_min'];
    temp_max = json['temp_max'];
    pressure = json['pressure'];
    humidity = json['humidity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temp'] = this.temp;
    data['feels_like'] = this.feels_like;
    data['temp_min'] = this.temp_min;
    data['temp_max'] = this.temp_max;
    data['pressure'] = this.pressure;
    data['humidity'] = this.humidity;
    return data;
  }
}

class wind {
  late double speed;
  late int deg;
  late double gust;

  wind({required this.speed, required this.deg, required this.gust});

  wind.fromJson(Map<String, dynamic> json) {
    speed = json['speed'];
    deg = json['deg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['speed'] = this.speed;
    data['deg'] = this.deg;
    return data;
  }
}