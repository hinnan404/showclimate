import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather/DataModel/ModelClasses.dart';

import 'package:weather/Utils/Constants.dart';

import 'CustomTabView.dart';

class Tabbar extends StatefulWidget {
  createState() => TabbarState();
}

class TabbarState extends State<Tabbar> with SingleTickerProviderStateMixin {

  List<String> data = [''];
  int initPosition = 0;
  bool isLoading = true;

  SharedPreferences? pref;
  late String weatherTemp,weatherWind,weatherType;
  void getPreferenceData() async{
    pref = await SharedPreferences.getInstance();
    setState(() {
      weatherTemp = pref!.getString(Constants.weatherTemp)!;
      weatherWind = pref!.getString(Constants.weatherWind)!;
      weatherType = pref!.getString(Constants.weatherType)!;
    });
  }

  //Requesting current location permission
  getCurrentLocation() {
    GeolocatorPlatform.instance.getCurrentPosition(desiredAccuracy: LocationAccuracy.best,)
        .then((Position position) {
          //if user allow then call Weather Api to get Weather Condition
          getWeatherInformation(position.latitude.toString(),
              position.longitude.toString());
    }).catchError((e) {
      print(e);
      //If deny then show Dialog Alert Dialog
      showAlertDialog();
    });
  }

  void showAlertDialog(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Alert",
            style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
          content: Text(
            "We need your current location for next process!",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
          actions: [
            TextButton(
              onPressed: (){
                //Closing App 
                Navigator.pop(context);
                SystemNavigator.pop();
              },
              child: Text(
                "OK",
                style: TextStyle(
                  color: Colors.blue,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  var response;
  late String respData;
  WeatherInformation?modelWeatherInformation;
  getWeatherInformation(String lat, String long) async {
    pref = await SharedPreferences.getInstance();
    String url = "http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=${Constants.weatherKey}&units=metric";

    response = await http.post(
      Uri.parse(url),
      body: json.encode(new Map()),
    );

    if (response.statusCode == 200) {
      var responseDate = json.decode(response.body);
      setState(() {
        modelWeatherInformation = new WeatherInformation.fromJson(responseDate);
        pref!.setString(Constants.weatherTemp, modelWeatherInformation!.mainModel.temp.toString());
        pref!.setString(Constants.weatherWind, modelWeatherInformation!.windModel.speed.toString());
        pref!.setString(Constants.weatherType, modelWeatherInformation!.weatherModel[0].main.toString());
        data.add(modelWeatherInformation!.name);
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getPreferenceData();
    getCurrentLocation();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SafeArea(
          child: isLoading ?  Container() :
          Container(
            child: CustomTabView(
              initPosition: initPosition,
              itemCount: data.length,
              tabBuilder: (context, index) => Tab(text: data[index]),
              pageBuilder: (context, index) => Center(child: Text(data[index])),
              onPositionChange: (index){
                initPosition = index;
              },
              onScroll: (position) => print('$position'),
              stub: Container(),
              mainModel: modelWeatherInformation!.mainModel,
              windModel: modelWeatherInformation!.windModel,
              weatherList: modelWeatherInformation!.weatherModel,
              location: modelWeatherInformation!.name,
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            addTabDailog();
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  TextEditingController? latController = new TextEditingController();
  TextEditingController? longController = new TextEditingController();
  void addTabDailog(){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10,),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 10,),
                TextField(
                  controller: latController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: "Latitude",
                    hintStyle: TextStyle(
                      fontSize: 14,
                      fontFamily: "medium",
                      color: Colors.grey,
                    ),
                  ),
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "medium",
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 10,),
                TextField(
                  controller: longController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: "Longitude",
                    hintStyle: TextStyle(
                      fontSize: 14,
                      fontFamily: "medium",
                      color: Colors.grey,
                    ),
                  ),
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: "medium",
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  width: 120,
                  child: MaterialButton(
                    minWidth: MediaQuery.of(context).size.width,
                    height: 40,
                    elevation: 2,
                    highlightElevation: 0,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    color: Colors.blue,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                    ),
                    onPressed: (){
                      if(latController!.text.isNotEmpty && longController!.text.isNotEmpty){
                        getWeatherInformation(latController!.text, longController!.text);
                        Navigator.pop(context);
                      }
                    },
                    child: Text("ADD",
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: "semiBold",
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15,),
              ],
            ),
          ),
        );
      }
    );
  }
}

