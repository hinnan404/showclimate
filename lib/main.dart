import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:weather/TabBar/Tabbar.dart';


Future main() async{
  WidgetsFlutterBinding.ensureInitialized();

  runApp(new MyApp());
}

// This app is a stateful, it tracks the user's current choice.
class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.white,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.dark,
      statusBarColor: Colors.black,
    ));

    return MaterialApp(
      title: "Weather ",
      initialRoute: '/Homepage',
      routes: {
        "/Homepage": (context) => Tabbar()
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
